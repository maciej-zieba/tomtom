import os

# Paths to data
DATA_PATH = '/home/datasets/tomtom/'

SAMPLE_NAME = 'MML-RUS-MOSKOVSKAYA-BACKLOG-FRONT_2017_03_09__16_43_30.SQLITE'

# Filename with arosa data
AROSA_NAME = 'arosa_result_RUS_2018_06_19__13_38_25.csv'

# Filename for transition matrix
TRANS_MATRIX_NAME = 'transition_matrix.csv'

# Path to arosa
AROSA_PATH = os.path.join(DATA_PATH, AROSA_NAME)
SAMPLE_PATH = os.path.join(DATA_PATH, SAMPLE_NAME)

# Size of the input images
MAX_X = 1920
MAX_Y = 1080

# Size of the bounding boxes
SPREAD_X = 300
SPREAD_Y = 450

# Should be arosa class detections included in deduplications
CONSIDER_CLASSES = False

# Thereshold values for distance acceptance
THRESHOLD_RES = 14.0

# Shifting variants for matching distances
SHIFT_VARIANTS = [(-1.0*SPREAD_X, -1.0*SPREAD_Y),
                  (-1.0*SPREAD_X, SPREAD_Y),
                  (SPREAD_X, -1.0*SPREAD_Y),
                  (SPREAD_X, SPREAD_Y)]

# Max value of distance considered in experiments
MAX_DIST = 10000000

# Use cuda for pytorch models
CUDA = True
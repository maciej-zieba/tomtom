# Algorithm for deduplication
We provide the final version of the deduplication script.

## Installation
The project requires couple of standard libraries, ```numpy```, ```scipy```, ```pandas```, ```Pillow```. For feature extraction purpose we use ```pytorch``` library together with ```torchvision``` (see ```requirements.txt``` for details).

## User documentation

The ```main.py``` script can be used to run the duplication on data located in ```DATA_PATH```.
Arosa file should be also provided under location ```AROSA_PATH```.

The deduplication procedure is performed by ```proceed_deduplication``` function. The function provides the modified Arosa files with two additional columns: ```importance_flag``` and ```track_id```.
The ```importance_flag``` takes 0 if the the sign is not observed for las time, and 1 otherwise.
The ```track_id``` represents id of the track signs. The same sign should have the same ```track_id``` value.
The output arosa files are saved in default folder ```results``` in ```DATA_PATH``` location.

There are several parameters for the deduplication method that are explained below:




The path to the datasets that containes *.SQLLITE files:
```DATA_PATH```

The path to the Arosa data:
```AROSA_PATH ```

The size of the bounding boxes used for feature extraction:
```SPREAD_X = 300```
```SPREAD_Y = 450```

We take 4 bounding boxes in comparing cropped images determined by the following shifting vectors:


```SHIFT_VARIANTS = [(-1.0*SPREAD_X, -1.0*SPREAD_Y),
                  (-1.0*SPREAD_X, SPREAD_Y),
                  (SPREAD_X, -1.0*SPREAD_Y),
                  (SPREAD_X, SPREAD_Y)]```


The boolean variable that if is set to True, we assume that Arosa correctly classified the signs and deduplication is based on class labels. If it is set on False, the deduplication method does not take classes into account and correct the classifications of Arosa:
```CONSIDER_CLASSES = False```


The acceptances threshold for treating the sign as different/same. If the value is high we tend to accept the possible pairs to be the same signs (risk of treating two different signs as the same sign). If the value is low, there is the risk of treating one signs as two different signs.
```THRESHOLD_RES = 14.0```

If cuda should be used for feature extraction.
```CUDA = True```
import math
import numpy as np
import os
import torchvision
import torch.nn as nn
import sqlite3

from utils import extract_image, filter_arosa_by_session
from settings import CONSIDER_CLASSES, CUDA, DATA_PATH, \
    MAX_DIST, MAX_X, SHIFT_VARIANTS, THRESHOLD_RES
from scipy.spatial.distance import cdist
from torch.autograd import Variable
from torchvision import transforms


IMAGE_NORMALIZATION_MEANS = [0.485, 0.456, 0.406]
IMAGE_NORMALIZATION_STDS = [0.229, 0.224, 0.225]

def create_resnet50():
    """
    The function reads and returns ResNET model
    for extracting features from image

    :return: ResNET model for with the last layer
    for feature extraction
    """
    cnn = torchvision.models.resnet50(pretrained=True)
    return nn.Sequential(*list(cnn.children())[:-1])


def preprocess_image():
    """
    The function defines the set of transformations that
    should be made, to transform image properly to obtain
    features from ResNET structure

    :return: Set of transformations to prepare image for
    input of the ResNET
    """
    normalize = transforms.Normalize(mean=IMAGE_NORMALIZATION_MEANS,
                                     std=IMAGE_NORMALIZATION_STDS)
    trans = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        normalize])

    return trans


def get_features_from_image(im, model):
    """
    The function returns features from image using
    specified model
    :param im: input image
    :param model: resNET model used for feature extraction
    :return: features extracted from single image
    """
    transform = preprocess_image()
    transformed_im = transform(im)
    transformed_im.unsqueeze_(-4)
    if CUDA:
        model = model.cuda()
        transformed_im = transformed_im.cuda()
    model.eval()
    batch_features = model(Variable(transformed_im))
    result_features = batch_features.cpu().data.numpy()
    return result_features


def get_central_point(data, index):
    """
    The function returns central point location
    of the detected sign with index 0
    :param data: pandas with Arosa data with detected signs
    :param index: id of the record, for which bounding box
    should be extracted
    :return:
    """
    return [data.at[index, 'bbox.x'] + 0.5*data.at[index, 'bbox.width'],
            data.at[index, 'bbox.y'] + 0.5*data.at[index, 'bbox.height']]


def get_features_from_data(data, model, cursor, shift_x, shift_y):
    """
    The function returns numpy array of features obtained from detected
    signs by Arosa that are stored in data. The features are taken from
    the bounding box composed of central point p_c and the point shifted
    by the vector [shift_x, shift_y]
    :param data: pandas with Arosa data with detected signs
    :param model: resNET model used for feature extraction
    :param cursor: connection cursor
    :param shift_x: shifting for x-axis
    :param shift_y: shifting for y-axis
    :return: 2D numpy array with extracted features
    """
    feature_vector = []
    for index, row in data.iterrows():
        frame_id = row['frameIdx']
        im = extract_image(cursor, frame_id)
        p_c = get_central_point(data, index)
        bbox = (min(p_c[0], p_c[0] + shift_x),
                min(p_c[1], p_c[1] + shift_y),
                max(p_c[0], p_c[0] + shift_x),
                max(p_c[1], p_c[1] + shift_y))
        im = im.crop(bbox)
        features = get_features_from_image(im, model)
        # ResNET returns extra dim so reshape to 2D is needed
        features = np.reshape(features,
                              (features.shape[0], features.shape[1]))
        feature_vector.append(features)
    return np.concatenate(feature_vector, axis=0)


def get_central_points(data):
    """
    The function stores central points in numpy
    array
    :param data: pandas with Arosa data to detected signs
    :return: numpy array with central points of detections
    """
    feature_vector = []
    for index, row in data.iterrows():
        p_c = get_central_point(data, index)
        feature_vector.append(np.array([[p_c[0], p_c[1]]]))
    return np.concatenate(feature_vector, axis=0)


def get_transition_indexing(data):
    """
    The function returns proper indexing to get
    the ids for transition matrix
    :param data: Arosa data
    :return: transition ids
    """
    points = np.floor(get_central_points(data) / 100.0).astype(int)
    coding = points[:, 1]*math.ceil(MAX_X/100.0) + points[:, 0]
    return coding


def get_transition_probs(data_current, data_next, trans_matrix):
    """
    The function return transition probabilities between frames
    :param data_current: Arosa data from current frame
    :param data_next: Arosa data from next frame
    :param trans_matrix: transition matrix
    :return: Numpy array with transition probabilities
    """
    current_codes = get_transition_indexing(data_current)
    future_codes = get_transition_indexing(data_next)
    probs = np.zeros((future_codes.shape[0], current_codes.shape[0]))
    for i in range(current_codes.shape[0]):
        for j in range(future_codes.shape[0]):
            if np.sum(trans_matrix[current_codes[i], :]) == 0:
                probs[j, i] = 0.0
            else:
                probs[j, i] = trans_matrix[current_codes[i], future_codes[j]]\
                                 / np.sum(trans_matrix[current_codes[i], :])
    return probs


def filter_by_distances(paired_distances, threshold):
    """
    The function returns matched pairs basing
    on distance matrix
    :param paired_distances: the numpy array that contains paris of distances
    between frames
    :param threshold: the acceptance threshold for the distance
    :return: the list of matched pairs between the frames
    """
    matched_pairs = []
    # Must be done in increasing distance order
    for _ in range(paired_distances.shape[0]):
        row_id = int(np.argmin(paired_distances) / paired_distances.shape[1])
        col_id = np.argmin(paired_distances) % paired_distances.shape[1]
        dist_current = paired_distances[row_id, col_id]
        paired_distances[row_id, :] = MAX_DIST
        paired_distances[:, col_id] = MAX_DIST
        if dist_current < threshold:
            matched_pairs.append((col_id, row_id))
    return matched_pairs


def get_matchings(data_current_frame, data_next_frame,
                  model, trans_matrix, cursor):
    """
    The function matches the detected objects between current and next frames
    :param data_current_frame: arosa objects in current frame
    :param data_next_frame: arosa objects in next frame
    :param model: resNET model used for feature extraction
    :param trans_matrix: transition matrix
    :param cursor: connection cursor
    :return: list of matched pairs
    """
    distances_global = np.zeros((len(SHIFT_VARIANTS),
                                 data_next_frame.shape[0],
                                 data_current_frame.shape[0]))
    for i, elem in enumerate(SHIFT_VARIANTS):
        f_current = get_features_from_data(data_current_frame,
                                           model, cursor, elem[0], elem[1])
        f_next = get_features_from_data(data_next_frame, model,
                                        cursor, elem[0], elem[1])
        distances_global[i, :, :] = cdist(f_next, f_current)
    distances_global = np.min(distances_global, axis=0)
    prob_transition = get_transition_probs(data_current_frame,
                                           data_next_frame,
                                           trans_matrix)
    distances_global = distances_global*(1.0 - prob_transition)
    matched_elements = filter_by_distances(distances_global, THRESHOLD_RES)
    return matched_elements


def get_instance_id_for_pair(data_current_frame, data_next_frame, model,
                             trans_matrix, cursor, instance_id):
    """
    The function finds the instance_id in next frame for
    the matched object from previous frame
    :param data_current_frame: arosa objects in current frame
    :param data_next_frame: arosa objects in next frame
    :param model: resNET model used for feature extraction
    :param trans_matrix: transition matrix
    :param cursor: connection cursor
    :param instance_id: id of the instance to be matched
    :return: list of matched pairs
    """
    next_instance_id = None
    data_current_frame = data_current_frame.reset_index()
    data_next_frame = data_next_frame.reset_index()
    if data_next_frame.shape[0] > 0:
        matched_elements = get_matchings(data_current_frame,
                                         data_next_frame, model,
                                         trans_matrix, cursor)
        id_current = data_current_frame[
            data_current_frame['instanceId'] == instance_id
        ].index.get_values()[0]
        future_ids = [i[1] for i in matched_elements if i[0] == id_current]
        if len(future_ids) > 0:
            next_instance_id = data_next_frame.at[future_ids[0], 'instanceId']
    return next_instance_id


def get_most_probable_class(data):
    """
    Retruns the most probable class from data
    :param data: arosa pandas
    :return: most probable sign class
    """
    grouped_data = data.groupby('category').mean()
    grouped_data = grouped_data[
        grouped_data['confidence'] == grouped_data['confidence'].max()]
    return grouped_data.index.tolist()[0]


def proceed_deduplication(sql_filename, arosa_file, model, trans_matrix):
    """
    Function proceed deduplication - provides two additional columns to
    the arosa file:
    importance_flag - represents deduplication result 1 - last sign,
    0 - no last sign
    track_id - id of the tracked sign, the last frame with the current
    track id represents the last frame, where the sign were observed
    :param sql_filename: name with the sql file to the considered session
    :param arosa_file: pandas with arosa data
    :param model: resNET model used for feature extraction
    :param trans_matrix: transition matrix
    :return: pandas with arosa data with deduplication and tracking
    results
    """
    sql_path = os.path.join(DATA_PATH, sql_filename)
    conn = sqlite3.connect(sql_path)
    cursor = conn.cursor()
    arosa_data = filter_arosa_by_session(arosa_file,
                                         sql_filename.split(".")[0])
    arosa_data['importance_flag'] = 0
    arosa_data['track_id'] = -1
    track_id = 0
    for index, row in arosa_data.iterrows():
        instance_id = row['instanceId']
        frame_id = row['frameIdx']
        category = row['category']
        if CONSIDER_CLASSES:
            class_case = arosa_data['category'] == category
        else:
            class_case = True
        arosa_current = arosa_data[(arosa_data['frameIdx'] == frame_id) &
                                   class_case]
        arosa_future = arosa_data[(arosa_data['frameIdx'] == frame_id + 1) &
                                  class_case]
        if arosa_data.at[index, 'track_id'] == -1:
            instance_id = get_instance_id_for_pair(arosa_current,
                                                   arosa_future,
                                                   model,
                                                   trans_matrix,
                                                   cursor,
                                                   instance_id)
            while instance_id is not None:
                frame_id += 1
                arosa_data.loc[(arosa_data['frameIdx'] == frame_id) &
                               (arosa_data['instanceId'] == instance_id),
                               'track_id'] = track_id
                arosa_current = arosa_data[
                    (arosa_data['frameIdx'] == frame_id) & class_case]
                arosa_future = arosa_data[
                    (arosa_data['frameIdx'] == frame_id + 1) & class_case]
                instance_id = get_instance_id_for_pair(arosa_current,
                                                       arosa_future,
                                                       model,
                                                       trans_matrix, cursor,
                                                       instance_id)
            arosa_data.at[index, 'track_id'] = track_id
            last_frame = arosa_data[
                arosa_data['track_id'] == track_id
            ]["frameIdx"].max()
            arosa_data.loc[(arosa_data['frameIdx'] == last_frame) &
                           (arosa_data['track_id'] == track_id),
                           'importance_flag'] = 1
            if not CONSIDER_CLASSES:
                best_category = get_most_probable_class(
                    arosa_data[arosa_data['track_id'] == track_id]
                )
                arosa_data.loc[(arosa_data['track_id'] == track_id),
                               'category'] = best_category
        track_id += 1
    return arosa_data

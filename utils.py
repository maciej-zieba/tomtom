import os
import pandas

from io import BytesIO
from PIL import Image
from settings import DATA_PATH


def extract_image(cursor, picture_id):
    """
    Function extract image from
    sql database by the cursor id
    :param cursor: cursor to database
    :param picture_id: id of the picture
    :return: extracted image in Image (PIL) format
    """
    sql = "SELECT ttvalue FROM camera_1_frames WHERE idx = :id"
    param = {'id': picture_id}
    cursor.execute(sql, param)
    ablob = cursor.fetchone()[0]
    im = Image.open(BytesIO(ablob))
    return im


def filter_arosa_by_session(arosa_file, session_name):
    """
    Filter arosa data by session id
    :param arosa_file: pandas with arosa data
    :param session_name: name of arosa session
    :return: filtered pandas with arosa session data
    """
    arosa_data = pandas.read_csv(arosa_file)
    arosa_data = arosa_data[arosa_data['session.name'] == session_name]
    arosa_data = arosa_data.sort_values(by=['frameIdx'])
    return arosa_data


def save_arosa_data(arosa_file, filename, folder='results'):
    """
    Save arosa data in csv file
    :param arosa_file: pandas file with arosa data
    :param filename: name of destination file
    :param folder: destination folder
    :return:
    """
    results_dir = os.path.join(DATA_PATH, folder)
    if not os.path.exists(results_dir):
        os.makedirs(results_dir)
    arosa_file.to_csv(os.path.join(results_dir, filename + '.csv'))

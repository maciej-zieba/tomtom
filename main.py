import numpy as np
import os

from models import create_resnet50, proceed_deduplication
from settings import AROSA_PATH, DATA_PATH, TRANS_MATRIX_NAME
from utils import save_arosa_data

# Reading the model for feature extraction
model = create_resnet50()

# Reading transition matrix
trans_matrix = np.genfromtxt(TRANS_MATRIX_NAME, delimiter=',')

# procced_tracking(SAMPLE_NAME, AROSA_PATH, model)
for filename in os.listdir(DATA_PATH):
    if filename.endswith(".SQLITE"):
        print(filename)
        try:
            arosa_data = proceed_deduplication(filename,
                                               AROSA_PATH,
                                               model,
                                               trans_matrix)
            save_arosa_data(arosa_data, filename)
        except IOError:
            print('Some problems with file' + filename)
